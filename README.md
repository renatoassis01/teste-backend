# Renato movie database

# O que esse projeto usa

 * express  
 * OvernightJS para annotations de rotas
 * typscript para desenvolvimento
 * jsonwebtoken para autenticação
 * joi para validação
 * typeorm para persistência de dados
 * prettier para formatação de código
 * eslint para lint de código
  
 > O projeto usa .env para desenvolvimento local

# requerimentos para desenvolvimento

 * docker & docker-compose
 * nodejs >= 12
 
 > O docker é necessário para rodar um banco local postgres de desenvolvimento 

#  como esse projeto esta estruturado

common: arquivos comum do projeto
modules: separação por responsabilidade
@types: arquivos de definição do typescript


# setup do projeto

```sh
 git clone https://gitlab.com/renatoassis01/teste-backend.git
 cd rmdb 
 npm install 
```

# como rodar o projeto

```sh
 docker-compose up --build -d
 npm run migrate:up
 npm run start:dev 
```

# observação
criar um usuário pela rota `{{BASE_URL}}/api/user` e ir no banco e setar a coluna admin para `true`

# rotas 
 1 - importar os arquivos na pasta do postman 

 2 - selecionar o ambiente `RMDB_DEV` antes de realizar as chamada.
 
 3 - Realizar login na rota `{{BASE_URL}}/api/auth/login`. Essa rota seta automaticamente o `{{token}}` no ambiente token do ambiente do postman


# O que ficou faltando desenvolver

 * avaliações dos usuário no filme.    
 
 > obs: A rota existe `{{BASE_URL}}/api/rating` e já barra se não for usuário.

 * media das avaliações de usuários por filme. 

 * implementar testes