const path = require('path')
const options = {
    type: 'postgres',
    name: 'default',
    host: process.env.POSTGRES_HOST || 'localhost',
    port: process.env.POSTGRES_PORT || 5432,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    synchronize: true,
    logging: true,
    entities: [path.join(__dirname, 'src/modules/**/models/*.ts'),
        './src/modules/rating/models/RatingModel.ts'
    ],
    migrations: [
        'src/database/migrations/*.ts',
    ],
    cli: {
        migrationsDir: 'src/database/migrations',
    },
}

module.exports = options