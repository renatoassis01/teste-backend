import { Server } from '@overnightjs/core';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import { Application } from 'express';
import logger from 'morgan';
import { AdminController } from './modules/admin/controllers/AdminController';
import { AuthController } from './modules/auth/controllers/AuthController';
import { MovieController } from './modules/movies/controllers/MovieController';
import { RatingController } from './modules/rating/controllers/RatingController';
import { UserContoller } from './modules/user/controllers/UserController';

export class SetupServer extends Server {
  constructor() {
    super();
    dotenv.config();
    this.init();
  }

  private init(): void {
    this.setupExpress();
    this.setupControllers();
  }

  private setupExpress(): void {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }))
    this.app.use(logger('dev'))
  }

  private setupControllers(): void {
    const userController = new UserContoller();
    const authController = new AuthController();
    const adminController = new AdminController();
    const movieController = new MovieController();
    const ratingController = new RatingController();
    this.addControllers([userController, authController, movieController, adminController, ratingController]);
  }

  public getApp(): Application {
    return this.app;
  }

  public start(): void {
    const PORT = process.env.APPLICATION_PORT;
    this.app.listen(PORT, () => {
      console.log('Server listening on port: ' + PORT);
    });
  }
}
