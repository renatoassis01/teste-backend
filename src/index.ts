import 'reflect-metadata';
import './config/aliasconfig';
import { SetupServer } from './Server';

const server = new SetupServer();
server.start();
