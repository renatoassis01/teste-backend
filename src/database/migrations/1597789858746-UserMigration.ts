import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserMigration1597789858746 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'CREATE TABLE "user" ( ID serial PRIMARY KEY, email VARCHAR, name VARCHAR, password VARCHAR, admin BOOLEAN, ENABLE BOOLEAN );'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DROP TABLE "user";');
  }
}
