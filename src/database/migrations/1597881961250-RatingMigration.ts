import { MigrationInterface, QueryRunner } from "typeorm";

export class RatingMigration1597881961250 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query(`  
        
        CREATE TABLE rating(
            ID serial PRIMARY KEY,
            moveId INT4 NOT NULL REFERENCES movie(ID) ON DELETE CASCADE,
            rating SMALLINT
        );`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query('DROP TABLE rating;')
    }

}
