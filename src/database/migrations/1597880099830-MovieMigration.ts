import { MigrationInterface, QueryRunner } from "typeorm";

export class MovieMigration1597880099830 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query(`
        
            CREATE TABLE movie (
            ID serial PRIMARY KEY,
            NAME VARCHAR NOT NULL,
            description VARCHAR NULL,
            gender VARCHAR NOT NULL,
            publish DATE NOT NULL,
            diretor VARCHAR NOT NULL 
        );
        
        `)

    }
    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('DROP TABLE movie;')
    }

}

