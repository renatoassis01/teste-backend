import { createConnection, Repository } from 'typeorm';
import { UserModel } from '../models/UserModel';

export class UserRepository {
  private userRepository: Repository<UserModel>;

  constructor() {
    createConnection().then((connection) => {
      this.userRepository = connection.getRepository(UserModel);
    });
  }

  public async create(user: UserModel): Promise<UserModel> {
    return await this.userRepository.save(user);
  }

  public async findByEmail(userEmail: string): Promise<UserModel | undefined> {
    return await this.userRepository.findOne({ email: userEmail });
  }

  public async findOne(userId: number): Promise<UserModel | undefined> {
    return await this.userRepository.findOne(userId);
  }

  public async update(user: UserModel): Promise<UserModel> {
    return await this.userRepository.save(user)

  }

  public async softDelete(user: UserModel): Promise<Boolean> {
    const result = await this.userRepository.update(user.id, {
      enable: false,
    });
    return result.affected !== undefined && result.affected > 0;
  }

  public async promoteAdmin(user: UserModel): Promise<Boolean> {
    const result = await this.userRepository.save({ ...user, admin: true });
    return result.admin;
  }
}
