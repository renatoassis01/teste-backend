import { NextFunction, Request, Response } from 'express';
import { CryptoUtils } from '../../../commom/utils/CryptoUtils';
import { UserResponseDTO } from '../dtos/UserResponseDTO';
import { UserModel } from '../models/UserModel';
import { UserRepository } from './UserRepository';

export class UserManager {
  private userRepository: UserRepository;
  constructor() {
    this.userRepository = new UserRepository();
  }

  public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const user = await this.userRepository.findByEmail(req.body.email);
      if (user !== undefined) res.status(409).json({ msg: 'user already exists' });
      else {
        const user = await this.userRepository.create({
          email: req.body.email,
          name: req.body.name,
          password: CryptoUtils.encrypt(req.body.password),
          admin: false,
          enable: true
        } as UserModel);
        res.status(200).json({ data: new UserResponseDTO(user) });
      }
    } catch (error) {
      return next(error);
    }
  }

  public async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = Number(req.params.id);
      const user = await this.userRepository.findOne(id);
      if (user === undefined) res.status(404).json({ msg: 'user not found' });
      else {

        const userUpdated = await this.userRepository.update({
          ...user,
          name: req.body.name
        });
        res.status(200).json({ data: new UserResponseDTO(userUpdated) });
      }
    } catch (error) {
      return next(error);
    }
  }

  public async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = Number(req.params.id);
      const user = await this.userRepository.findOne(id);
      if (user === undefined) {
        res.status(404).json({ msg: 'user not found' });
      } else {
        await this.userRepository.softDelete(user);
        res.status(200).json({ msg: 'removed user' });
      }
    } catch (error) {
      return next(error);
    }
  }

  public async findById(userId: number): Promise<UserModel | undefined> {
    return await this.userRepository.findOne(userId);
  }

  public async findByEmail(userEmail: string): Promise<UserModel | undefined> {
    return await this.userRepository.findByEmail(userEmail);
  }

  public async promote(user: UserModel): Promise<Boolean> {
    return await this.userRepository.promoteAdmin(user);
  }
}
