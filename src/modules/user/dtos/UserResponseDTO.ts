import { UserModel } from '../models/UserModel';

export class UserResponseDTO {
  private id: number;
  private email: string;
  private admin: boolean;
  private name: string;

  constructor(data: UserModel) {
    this.id = data.id;
    this.name = data.name;
    this.email = data.email;
    this.admin = data.admin;
  }
}
