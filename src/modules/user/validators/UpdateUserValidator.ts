import Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

export class UpdateUserValidator {
  private static schema = Joi.object({
    name: Joi.string().optional(),
  });

  public static validator(req: Request, res: Response, next: NextFunction): void {
    const result = UpdateUserValidator.schema.validate(req.body);
    if (result.error?.isJoi) res.status(422).json({ msg: result.error.details });
    else next()
  }
}
