import Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

export class CreateUserValidator {
  private static schema = Joi.object({
    email: Joi.string().required().email(),
    name: Joi.string().required(),
    password: Joi.string().required(),
  });

  public static validator(req: Request, res: Response, next: NextFunction): void {
    const result = CreateUserValidator.schema.validate(req.body);
    if (result.error?.isJoi) res.status(422).json({ msg: result.error.details });
    else next();
  }
}
