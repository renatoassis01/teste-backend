import { Controller, Delete, Middleware, Post, Put } from '@overnightjs/core';
import { NextFunction, Request, Response } from 'express';
import { JwtManager } from '../../../commom/middlewares/jwt/JwtManager';
import { UserManager } from '../services/UserManager';
import { CreateUserValidator } from '../validators/CreateUserValidator';
import { UpdateUserValidator } from '../validators/UpdateUserValidator';

@Controller('api/user')
export class UserContoller {
  private _userManager: UserManager;
  constructor() {
    this._userManager = new UserManager();
  }

  @Post()
  @Middleware(CreateUserValidator.validator)
  public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    await this._userManager.create(req, res, next);
  }

  @Put(':id')
  @Middleware([JwtManager.authenticate, UpdateUserValidator.validator])
  public async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    await this._userManager.update(req, res, next);
  }

  @Delete(':id')
  @Middleware(JwtManager.authenticate)
  public async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
    await this._userManager.delete(req, res, next);
  }
}
