import Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

export class LoginUserValidator {
  private static schema = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
  });


  public static validator(req: Request, res: Response, next: NextFunction): void {
    const result = LoginUserValidator.schema.validate(req.body);
    if (result.error?.isJoi) res.status(422).json({ msg: result.error.details });
    else next();
  }
}
