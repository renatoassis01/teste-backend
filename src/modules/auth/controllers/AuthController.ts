import { Controller, Middleware, Post } from '@overnightjs/core';
import { NextFunction, Request, Response } from 'express';
import { AuthManager } from '../services/AuthManager';
import { LoginUserValidator } from '../validators/LoginUserValidator';

@Controller('api/auth')
export class AuthController {
  private _authManager: AuthManager;
  constructor() {
    this._authManager = new AuthManager();
  }

  @Post('login')
  @Middleware(LoginUserValidator.validator)
  public async login(req: Request, res: Response, next: NextFunction): Promise<void> {
    await this._authManager.login(req, res, next);
  }
}
