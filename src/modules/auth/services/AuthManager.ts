import { NextFunction, Request, Response } from 'express';
import { JwtManager } from '../../../commom/middlewares/jwt/JwtManager';
import { CryptoUtils } from '../../../commom/utils/CryptoUtils';
import { UserRepository } from '../../user/services/UserRepository';
import { AuthResponseDTO } from '../dtos/AuthResponseDTO';

export class AuthManager {
  private userRepository: UserRepository;
  constructor() {
    this.userRepository = new UserRepository();
  }

  public async login(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const user = await this.userRepository.findByEmail(req.body.email);
      if (!user) res.status(404).json({ msg: 'user not found' });
      if (!!user) {
        if (CryptoUtils.compare(req.body.password, user.password)) {
          const token = JwtManager.getToken(user.email, user.admin);
          res.status(200).json({ data: new AuthResponseDTO(token) });
        } else {
          res.status(422).json({ msg: 'password wrong' });
        }
      }
    } catch (error) {
      next(error);
    }
  }
}
