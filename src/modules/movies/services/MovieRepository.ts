import { createConnection, Repository } from "typeorm";
import { ISearchMovie } from "../interfaces/ISearchMovie";
import { MovieModel } from "../models/MovieModel";

export class MovieRepository {
    private _movieRepository: Repository<MovieModel>;

    constructor() {
        createConnection().then((connection) => {
            this._movieRepository = connection.getRepository(MovieModel);
        });
    }

    public async create(movie: MovieModel): Promise<MovieModel> {
        return await this._movieRepository.save(movie);
    }

    public async search(filters: ISearchMovie): Promise<MovieModel[]> {
        const query = this._movieRepository
            .createQueryBuilder('movie')
        if (!!filters.name)
            query.orWhere(`movie.name ilike '%${filters.name}%'`)

        //full-searchtext é melhor para buscar colunas de texto
        if (!!filters.diretor)
            query.orWhere(`movie.diretor ilike '%${filters.diretor}%'`)

        if (!!filters.gender)
            query.orWhere(`movie.gender ilike '%${filters.gender}%'`)

        if (!!filters.skip)
            query.skip(filters.skip)

        if (!!filters.take)
            query.take(filters.take)

        return await query.getMany()

    }

    public async findByName(movieName: string): Promise<MovieModel | undefined> {
        return await this._movieRepository.findOne({
            name: movieName
        });
    }

    public async findOne(movieId: number): Promise<MovieModel | undefined> {
        return await this._movieRepository.findOne({
            id: movieId
        });
    }
}