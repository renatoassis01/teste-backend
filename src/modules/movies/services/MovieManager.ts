import { NextFunction, Request, Response } from "express";
import { MovieListResponseDTO } from "../dtos/MovieListResponseDTO";
import { MovieResponseDTO } from "../dtos/MovieResponseDTO";
import { ISearchMovie } from "../interfaces/ISearchMovie";
import { MovieModel } from "../models/MovieModel";
import { MovieRepository } from "./MovieRepository";

export class MovieManager {

    private _movieRepository: MovieRepository
    constructor() {
        this._movieRepository = new MovieRepository()
    }

    public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const movie = await this._movieRepository.findByName(req.body.name);
            if (movie !== undefined) res.status(409).json({ msg: 'movie already exists' });
            else {
                const movie = await this._movieRepository.create({
                    name: req.body.name,
                    description: req.body.description,
                    gender: req.body.gender,
                    publish: req.body.publish,
                    diretor: req.body.diretor
                } as MovieModel);
                res.status(200).json({ data: new MovieResponseDTO(movie) });
            }
        } catch (error) {
            return next(error);
        }
    }

    public async search(req: Request, res: Response, next: NextFunction): Promise<void> {
        const { name, gender, diretor, take, skip } = req.query
        const movies = await this._movieRepository.search({ name, gender, diretor, take, skip } as ISearchMovie)
        res.status(200).json({ data: new MovieListResponseDTO(movies) });
    }

    public async findById(movieId: number): Promise<MovieModel | undefined> {
        return await this._movieRepository.findOne(movieId);
    }



}