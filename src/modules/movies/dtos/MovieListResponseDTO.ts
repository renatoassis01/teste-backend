import { MovieModel } from "../models/MovieModel";
import { MovieResponseDTO } from "./MovieResponseDTO";

export class MovieListResponseDTO {

    private list: MovieResponseDTO[]
    private total: number

    constructor(data: MovieModel[]) {
        this.list = !!data && data.length > 0 ? data.map(s => new MovieResponseDTO(s)) : []
        this.total = !!data && data.length > 0 ? data.length : 0
    }
}