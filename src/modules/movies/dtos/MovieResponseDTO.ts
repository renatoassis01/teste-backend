import { MovieModel } from "../models/MovieModel"

export class MovieResponseDTO {

    private id: number
    private name: string
    private description: string | null
    private gender: string
    private publish: Date
    private diretor: string

    constructor(data: MovieModel) {

        this.id = data.id
        this.name = data.name
        this.description = !!data.description ? data.description : null
        this.gender = data.gender
        this.publish = data.publish
        this.diretor = data.diretor

    }
}