import { Controller, Get, Middleware, Post } from '@overnightjs/core';
import { NextFunction, Request, Response } from 'express';
import { JwtManager } from '../../../commom/middlewares/jwt/JwtManager';
import { IsUserAdminValidator } from '../../../commom/validators/IsUserAdminValidator';
import { MovieManager } from '../services/MovieManager';
import { CreateMovieValidator } from '../validators/CreateMovieValidator';
import { SearchMovieValidator } from '../validators/SearchMovieValidator';

@Controller('api/movie')
export class MovieController {
    private _movieManager: MovieManager;
    constructor() {
        this._movieManager = new MovieManager();
    }

    @Post()
    @Middleware([
        JwtManager.authenticate,
        IsUserAdminValidator.validator,
        CreateMovieValidator.validator,
    ])
    public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        await this._movieManager.create(req, res, next);
    }

    @Get()
    @Middleware(SearchMovieValidator.validator)
    public async search(req: Request, res: Response, next: NextFunction): Promise<void> {
        await this._movieManager.search(req, res, next);
    }
}
