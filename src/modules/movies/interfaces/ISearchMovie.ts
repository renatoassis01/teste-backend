export interface ISearchMovie {

    name?: string,
    gender?: string,
    diretor?: string,
    take?: number,
    skip?: number,
}