import { Column, Entity, PrimaryGeneratedColumn } from "typeorm"

@Entity('movie')
export class MovieModel {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar' })
    name: string

    @Column({ type: 'text' })
    description: string

    @Column({ type: 'varchar' })
    gender: string

    @Column({ type: 'date' })
    publish: Date

    @Column({ type: 'varchar' })
    diretor: string

    // @OneToMany(() => RatingModel, (rating) => rating.movie)
    // ratings?: RatingModel[]

}