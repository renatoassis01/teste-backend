import Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

export class SearchMovieValidator {

    private static schema = Joi.object({
        name: Joi.string().optional(),
        gender: Joi.string().optional(),
        diretor: Joi.string().optional(),
        take: Joi.number().optional(),
        skip: Joi.number().optional()
    });

    public static validator(req: Request, res: Response, next: NextFunction): void {
        const result = SearchMovieValidator.schema.validate(req.body);
        if (result.error?.isJoi) res.status(422).json({ msg: result.error.details });
        else next()
    }

}