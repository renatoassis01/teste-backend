import Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

export class CreateMovieValidator {

    private static schema = Joi.object({
        name: Joi.string().required(),
        description: Joi.string().optional(),
        gender: Joi.string().required(),
        publish: Joi.date().required(),
        diretor: Joi.string().required()
    });

    public static validator(req: Request, res: Response, next: NextFunction): void {
        const result = CreateMovieValidator.schema.validate(req.body);
        if (result.error?.isJoi) res.status(422).json({ msg: result.error.details });
        else next()
    }

}