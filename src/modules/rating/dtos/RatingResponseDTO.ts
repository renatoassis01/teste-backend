import { RatingModel } from "../models/RatingModel"

export class RatingResponseDTO {

    private id: number
    private movieId: number
    private rating: number

    constructor(data: RatingModel) {
        this.id = data.id
        this.movieId = data.movieId
        this.rating = data.rating
    }
}