import { Controller, Middleware, Post } from '@overnightjs/core';
import { NextFunction, Request, Response } from 'express';
import { JwtManager } from '../../../commom/middlewares/jwt/JwtManager';
import { IsUserValidator } from '../../../commom/validators/IsUserValidator';
import { RatingManager } from '../services/RatingManager';
import { RatingMovieValidator } from '../validators/RatingMovieValidator';

@Controller('api/rating')
export class RatingController {
    private _ratingManager: RatingManager;
    constructor() {
        this._ratingManager = new RatingManager();
    }

    @Post()
    @Middleware([
        JwtManager.authenticate,
        IsUserValidator.validator,
        RatingMovieValidator.validator,
    ])
    public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        await this._ratingManager.create(req, res, next);
    }

}
