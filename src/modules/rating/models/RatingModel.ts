import { Column, Entity, PrimaryGeneratedColumn } from "typeorm"

Entity('rating')
export class RatingModel {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'int4' })
    movieId: number

    @Column({ type: 'smallint' })
    rating: number

}