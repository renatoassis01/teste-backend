import Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

export class RatingMovieValidator {

    private static schema = Joi.object({
        movieId: Joi.number().required(),
        rating: Joi.number().required().min(0).max(4),
    });

    public static validator(req: Request, res: Response, next: NextFunction): void {
        const result = RatingMovieValidator.schema.validate(req.body);
        if (result.error?.isJoi) res.status(422).json({ msg: result.error.details });
        else next()
    }

}