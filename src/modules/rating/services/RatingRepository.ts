import { createConnection, Repository } from "typeorm";
import { RatingModel } from "../models/RatingModel";

export class RatingRepository {
    private _ratingRepository: Repository<RatingModel>;

    constructor() {
        createConnection().then((connection) => {
            this._ratingRepository = connection.getRepository(RatingModel);
        });
    }

    public async create(rating: RatingModel): Promise<RatingModel> {
        return await this._ratingRepository.save(rating);
    }



}