import { NextFunction, Request, Response } from "express";
import { MovieManager } from "../../movies/services/MovieManager";
import { RatingResponseDTO } from "../dtos/RatingResponseDTO";
import { RatingModel } from "../models/RatingModel";
import { RatingRepository } from "./RatingRepository";

export class RatingManager {

    private _ratingRepository: RatingRepository
    private _movieManager: MovieManager
    constructor() {
        this._ratingRepository = new RatingRepository()
        this._movieManager = new MovieManager()
    }

    public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const movie = await this._movieManager.findById(req.body.movieId);
            if (movie === undefined) res.status(404).json({ msg: 'movie not found' });
            else {
                const rating = await this._ratingRepository.create({
                    movieId: req.body.movieId,
                    rating: req.body.rating
                } as RatingModel);
                res.status(200).json({ data: new RatingResponseDTO(rating) });
            }
        } catch (error) {
            return next(error);
        }
    }

}