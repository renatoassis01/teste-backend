import { Controller, Middleware, Post } from '@overnightjs/core';
import { NextFunction, Request, Response } from 'express';
import { JwtManager } from '../../../commom/middlewares/jwt/JwtManager';
import { IsUserAdminValidator } from '../../../commom/validators/IsUserAdminValidator';
import { AdminManager } from '../services/AdminManager';
import { PromoteUserValidator } from '../validators/PromoteUserValidator';

@Controller('api/admin')
export class AdminController {
  private _userManager: AdminManager;
  constructor() {
    this._userManager = new AdminManager();
  }

  @Post('promote')
  @Middleware([
    JwtManager.authenticate,
    IsUserAdminValidator.validator,
    PromoteUserValidator.validator,
  ])
  public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    this._userManager.promoteUserToAdmin(req, res, next);
  }
}
