import { NextFunction, Request, Response } from 'express';
import { UserManager } from '../../user/services/UserManager';

export class AdminManager {
  private userManager: UserManager;
  constructor() {
    this.userManager = new UserManager();
  }

  public async promoteUserToAdmin(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const { userId } = req.body;
      const user = await this.userManager.findById(userId);
      if (user === undefined) res.status(404).json({ msg: 'user not found' });
      else {
        const isAdmin = await this.userManager.promote(user);
        if (isAdmin) res.status(200).json({ msg: 'this user is now admin' });
      }
    } catch (error) {
      next(error);
    }
  }
}
