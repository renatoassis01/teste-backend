import Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

export class PromoteUserValidator {
  private static schema = Joi.object({
    userId: Joi.number().required(),
  });

  public static validator(req: Request, res: Response, next: NextFunction) {
    const result = PromoteUserValidator.schema.validate(req.body);
    if (result.error?.isJoi) res.status(422).json({ msg: result.error.details });
    else next();
  }
}
