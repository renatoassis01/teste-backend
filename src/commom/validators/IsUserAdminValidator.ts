import { NextFunction, Request, Response } from 'express';

export class IsUserAdminValidator {
  public static validator(req: Request, res: Response, next: NextFunction): void {
    if (!req.isAdmin) res.status(422).json({ msg: 'admin is required' });
    else next();
  }
}
