import { NextFunction, Request, Response } from 'express';

export class IsUserValidator {
    public static validator(req: Request, res: Response, next: NextFunction): void {
        if (req.isAdmin) res.status(422).json({ msg: 'user is required' });
        else next();
    }
}
