import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
export class JwtManager {
  public static getToken(email: string, isAdmin: boolean): string {
    return jwt.sign({ email, isAdmin }, process.env.APPLICATION_SECRET!);
  }

  public static authenticate(req: Request, res: Response, next: NextFunction) {
    const authHeader = req.headers.authorization;

    if (authHeader) {
      const token = authHeader.split(' ')[1];

      jwt.verify(token, process.env.APPLICATION_SECRET!, (err) => {
        if (err) {
          res.sendStatus(403);
        }

        const value = jwt.decode(token, { json: true });
        req.isAdmin = !!value && value.isAdmin ? Boolean(value.isAdmin) : false;
        next();
      });
    } else {
      res.sendStatus(401);
    }
  }
}
